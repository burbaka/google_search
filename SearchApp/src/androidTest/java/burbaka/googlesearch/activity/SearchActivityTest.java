package burbaka.googlesearch.activity;

import android.content.Context;
import android.content.Intent;

import org.junit.Before;
import org.junit.Test;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.uiautomator.By;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.Until;

public class SearchActivityTest {

  private static final long TIMEOUT = 5000;
  private static final String APP_PACKAGE = "burbaka.googlesearch";

  UiDevice mDevice;

  @Before
  public void setUp() {
    mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
    mDevice.pressHome();
  }

  @Test
  public void intentWasSend_AppStarted() {
    // Arrange.
    Context context = ApplicationProvider.getApplicationContext();
    Intent intent = context.getPackageManager()
        .getLaunchIntentForPackage(APP_PACKAGE);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

    // Act.
    context.startActivity(intent);

    // Assert
    mDevice.wait(Until.hasObject(By.pkg(APP_PACKAGE).depth(0)), TIMEOUT);
  }

}