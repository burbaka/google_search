package burbaka.googlesearch;

import android.app.Application;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;

/**
 * Custom application class for initializing project specific libraries, etc.
 */
public class SearchApplication extends Application {

    private static String TAG = SearchApplication.class.getSimpleName();

    private static String DEFAULT_VOLLEY_TAG = "Default Volley Request";

    /**
     * A singleton instance of the application class for easy access in other places
     */
    private static SearchApplication sInstance;

    private RequestQueue mRequestQueue;

    /**
     * @return ApplicationController singleton instance
     */
    public static synchronized SearchApplication getInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "onCreate()");

        sInstance = this;
    }


    /**
     * @return The Volley Request queue, the queue will be created if it is null
     */
    public RequestQueue getRequestQueue() {
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (mRequestQueue == null) {
            Log.d(TAG, "Request queue initialization");
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    /**
     * Adds the specified request to the global queue, if tag is not specified
     * then it is used else Default TAG is used.
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? DEFAULT_VOLLEY_TAG : tag);

        VolleyLog.d("Adding request to queue: %s", req.getUrl());

        getRequestQueue().add(req);
    }

    /**
     * Adds the specified request to the global queue using the Default TAG.
     */
    public <T> void addToRequestQueue(Request<T> req) {
        addToRequestQueue(req, null); // null parameter ensures DEFAULT_VOLLEY_TAG to be set
    }

    /**
     * Cancels all pending requests scheduled with the Default TAG.
     */
    public void cancelPendingRequests() {
        mRequestQueue.cancelAll(DEFAULT_VOLLEY_TAG);
    }

    /**
     * Cancels all pending requests by the specified TAG.
     *
     * @param tag Tag which was used to schedule request
     */
    public void cancelPendingRequests(String tag) {
        if (mRequestQueue != null) {
            if (TextUtils.isEmpty(tag)) {
                mRequestQueue.cancelAll(DEFAULT_VOLLEY_TAG);
            } else {
                mRequestQueue.cancelAll(tag);
            }
        }
    }

}
