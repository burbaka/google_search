package burbaka.googlesearch.activity;

import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import burbaka.googlesearch.R;
import burbaka.googlesearch.activity.fragment.SearchResultFragment;

public class MainActivity extends FragmentActivity {

    private static String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate(Bundle savedInstanceState)");

        setContentView(R.layout.activity_main);


        Fragment searchFragment = new SearchResultFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.layout_rootView, searchFragment, getString(R.string.fragmentTag_searchFragment)).commit();
    }

}
