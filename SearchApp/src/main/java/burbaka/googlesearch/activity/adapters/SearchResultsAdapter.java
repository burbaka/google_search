package burbaka.googlesearch.activity.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.FragmentActivity;
import burbaka.googlesearch.R;

/**
 * Used for map search results to UI views
 */
public class SearchResultsAdapter extends BaseAdapter {

    private static String TAG = SearchResultsAdapter.class.getSimpleName();


    private final FragmentActivity mActivity;
    private final List<SearchResult> mSearchResults;


    public SearchResultsAdapter(FragmentActivity activity, List<SearchResult> mSearchData) {
        Log.v(TAG, "SearchResultsAdapter(FragmentActivity activity, List<SearchResult> mSearchData)");

        mSearchResults = new ArrayList<>(mSearchData);
        mActivity = activity;
    }


    @Override
    public int getCount() {
        return mSearchResults.size();
    }

    @Override
    public Object getItem(int position) {
        return mSearchResults.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        SearchResult searchResult = mSearchResults.get(position);

        if (convertView == null) {
            holder = new ViewHolder();

            LayoutInflater layoutInflater = mActivity.getLayoutInflater();
            convertView = layoutInflater.inflate(R.layout.list_item_search_result, parent, false);

            holder.vTextSearchResultTitle = (TextView) convertView.findViewById(R.id.text_searchTitle);
            holder.vTextSearchResultText = (TextView) convertView.findViewById(R.id.text_searchText);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.vTextSearchResultTitle.setText(searchResult.mTitle);
        holder.vTextSearchResultText.setText(searchResult.mResultText);

        return convertView;
    }

    /**
     * Set new data for current adapter and updates the UI list
     */
    public void setNewData(List<SearchResult> newData){
        if (newData != null){
            mSearchResults.clear();
            mSearchResults.addAll(newData);
            notifyDataSetChanged();
        }
    }


    public static class SearchResult {

        final private String mTitle;
        final private String mResultText;

        private String mLink;

        public SearchResult(String title, String result){
            mTitle = title;
            mResultText = result;
        }

        public void setLink(String link) {
            mLink = link;
        }

        public String getLink() {
            return mLink;
        }
    }


    private static class ViewHolder {
        private TextView vTextSearchResultTitle;
        private TextView vTextSearchResultText;

    }
}
