package burbaka.googlesearch.activity.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import burbaka.googlesearch.GlobalValues;
import burbaka.googlesearch.R;
import burbaka.googlesearch.SearchApplication;
import burbaka.googlesearch.activity.adapters.SearchResultsAdapter;

public class SearchResultFragment extends Fragment implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONObject> {

    private static String TAG = "SearchResult";

    private EditText vEdtSearch;
    private Button vBtnStartSearch;
    private ListView vListSearchResults;

    private List<SearchResultsAdapter.SearchResult> mSearchResults;
    private AsyncTask<JSONObject, Object, List<SearchResultsAdapter.SearchResult>> mParsingResponseTask;
    private SearchResultsAdapter mSearchAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate(Bundle savedInstanceState)");

        mSearchResults = new ArrayList<SearchResultsAdapter.SearchResult>();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Log.i(TAG, "onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)");

        View rootView = inflater.inflate(R.layout.fragment_search_screen, container, false);

        vEdtSearch = (EditText) rootView.findViewById(R.id.edt_searchQuery);

        vBtnStartSearch = (Button) rootView.findViewById(R.id.btn_startSearch);
        vBtnStartSearch.setOnClickListener(this);

        vListSearchResults = (ListView) rootView.findViewById(R.id.list_searchResults);
        vListSearchResults.setScrollingCacheEnabled(true);
        vListSearchResults.setFastScrollEnabled(true);
        mSearchAdapter = new SearchResultsAdapter(getActivity(), new ArrayList<SearchResultsAdapter.SearchResult>());
        vListSearchResults.setAdapter(mSearchAdapter);
        vListSearchResults.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.v(TAG, "onItemClick(AdapterView<?> parent, View view, int position, long id)");

                SearchResultsAdapter.SearchResult searchItem = mSearchResults.get(position);
                Uri uri = Uri.parse(searchItem.getLink());
                Intent intent = new Intent(Intent.ACTION_VIEW).setData(uri);
                getActivity().startActivity(intent);
            }
        });


        return rootView;
    }

    @Override
    public void onAttach(@NonNull final Context context) {
        super.onAttach(context);
        Log.i(TAG, "onAttach(Context context)");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.i(TAG, "onDetach()");
    }


    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop()");

        if (mParsingResponseTask != null) {
            mParsingResponseTask.cancel(true);
        }
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick(View v)");

        switch (v.getId()) {
            case R.id.btn_startSearch:
                Log.v(TAG, "Start new search request.");

                String searchText = vEdtSearch.getText().toString();
                searchText = searchText.replace(" ", "%2B");

                final String queryArguments = "key=" + GlobalValues.API_KEY + "&cx=" + GlobalValues.SEARCH_ENGINE + "&q=" + searchText + GlobalValues.SPECIFIC_REPLY_FIELDS;
                final String requestUrl = GlobalValues.GOOGLE_SEARCH_URL + queryArguments;

                JsonObjectRequest request = new JsonObjectRequest(requestUrl, null, this, this);
                SearchApplication.getInstance().addToRequestQueue(request);


                break;
            default:
                Log.w(TAG, "Unknown view was clicked");
                break;
        }

    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        Log.e(TAG, volleyError.getMessage(), volleyError.getCause());
    }

    @Override
    public void onResponse(JSONObject response) {
        Log.d(TAG, "onResponse(JSONObject response)");

        mParsingResponseTask = new AsyncTask<JSONObject, Object, List<SearchResultsAdapter.SearchResult>>() {
            @Override
            protected List<SearchResultsAdapter.SearchResult> doInBackground(JSONObject... params) {
                return parseJsonServerResponse(params[0]);
            }

            @Override
            protected void onPostExecute(List<SearchResultsAdapter.SearchResult> searchResults) {
                super.onPostExecute(searchResults);
                if (!isCancelled()) {
                    mSearchResults.addAll(searchResults);
                    mSearchAdapter.setNewData(searchResults);
                }
            }
        };
        mParsingResponseTask.execute(response);

    }

    private List<SearchResultsAdapter.SearchResult> parseJsonServerResponse(JSONObject serverResponse) {
        List<SearchResultsAdapter.SearchResult> searchResults = new ArrayList<>();

        try {
            JSONArray searchItems = serverResponse.getJSONArray("items");
            for (int i = 0; i < searchItems.length(); i++) {
                JSONObject newJSONObj;
                newJSONObj = searchItems.getJSONObject(i);

                String title = newJSONObj.getString("title");
                String snippet = newJSONObj.getString("snippet");
                String link = newJSONObj.getString("link");

                SearchResultsAdapter.SearchResult searchItem = new SearchResultsAdapter.SearchResult(title, snippet);
                searchItem.setLink(link);

                searchResults.add(searchItem);
            }

        } catch (JSONException e) {
            Log.e(TAG, "error while parsing response JSON. ", e);
        }

        return searchResults;
    }
}
